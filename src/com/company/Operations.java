package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Operations implements StackOperations {

    private List<String> stackList = new ArrayList<>();
        @Override
        public List<String> get() {
            List<String> removedElementsList = new ArrayList<>();
            for(int i=stackList.size()-1;i>=0;i--){
                removedElementsList.add(stackList.get(i));
                stackList.remove(i);
            }
            return removedElementsList;
        }

        @Override
        public Optional<String> pop() {
            Optional<String> tmpElement = Optional.ofNullable(stackList.remove(stackList.size() - 1));
            return tmpElement;
        }

        @Override
        public void push(String item) {
            stackList.add(item);
        }
    }
