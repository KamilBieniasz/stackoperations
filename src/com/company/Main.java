package com.company;

public class Main {

    public static void main(String[] args) {
	    Operations operations = new Operations();
	    operations.push("first");
	    operations.push("second");
	    operations.push("third");
	    System.out.println(operations.pop());
		System.out.println(operations.pop());
		System.out.println(operations.pop());

		operations.push("first");
		operations.push("second");
		operations.push("third");
		for(String element:  operations.get()){
			System.out.println(element);
		}
    }
}
